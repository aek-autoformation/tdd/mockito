package com.autoformattion.TDDspring.data;

public interface SomeDataService {
    int[] retrieveAllData();
}
