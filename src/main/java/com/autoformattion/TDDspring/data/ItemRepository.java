package com.autoformattion.TDDspring.data;

import com.autoformattion.TDDspring.model.Item;

import org.springframework.data.jpa.repository.JpaRepository;

// this will be the database proxy, we do not care which type under the hood
// It will provide us with standard methods such findAll, findOne ...
public interface ItemRepository extends JpaRepository<Item, Integer> {
    
}
