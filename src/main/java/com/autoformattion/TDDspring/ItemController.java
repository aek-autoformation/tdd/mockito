package com.autoformattion.TDDspring;

import java.util.List;

import com.autoformattion.TDDspring.business.ItemBusinessService;
import com.autoformattion.TDDspring.model.Item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemController {
    
    @Autowired
    private ItemBusinessService businessService;

    @GetMapping("/dummy-item")
    public Item dummyItem() {
        return new Item(1,"Ball", 10, 100);
    }

    @GetMapping("/item-from-business-service")
    public Item itemFromBusinessService() {
        return businessService.retrieveHardcodedItem();
    }

    //retrieve data from business service
    @GetMapping("/all-items-from-database")
    public List<Item> retriveAllItems(){
        return businessService.retrieveAllItems();
    }
}