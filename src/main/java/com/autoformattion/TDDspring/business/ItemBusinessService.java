package com.autoformattion.TDDspring.business;

import java.util.List;

import com.autoformattion.TDDspring.data.ItemRepository;
import com.autoformattion.TDDspring.model.Item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemBusinessService {

    @Autowired
    private ItemRepository repository;

    public Item retrieveHardcodedItem(){
        return new Item(1,"Ball",10,100);
    }

    public List<Item> retrieveAllItems(){
        //repository is just an interface
        //combine methods to create a business logic

        //getting data from repository using standard methos
        List<Item> items = repository.findAll();

        //Creating Business logic in value field
        for(Item item:items){
            item.setValue(item.getPrice() * item.getQuantity());
        }
        
        return items;
    }
}
