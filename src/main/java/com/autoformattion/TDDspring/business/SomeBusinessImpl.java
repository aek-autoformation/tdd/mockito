package com.autoformattion.TDDspring.business;

import java.util.Arrays;

import com.autoformattion.TDDspring.data.SomeDataService;

public class SomeBusinessImpl {

    private SomeDataService someDataService;


    public SomeDataService getSomeDataService() {
        return someDataService;
    }

    public void setSomeDataService(SomeDataService someDataService) {
        this.someDataService = someDataService;
    }

    public int caclulateSum(int[] data) {
        return Arrays.stream(data).reduce(Integer::sum).orElse(0);
    }

    public int calculateSumUsingDataService(){
        int sum = 0;
        int[] data = someDataService.retrieveAllData();
        for(int value:data){
            sum += value;
        }

        return sum;
    }
}
