package com.autoformattion.TDDspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TdDspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TdDspringApplication.class, args);
	}

}
