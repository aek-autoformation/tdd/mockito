package com.autoformattion.TDDspring.controller;


import com.autoformattion.TDDspring.ItemController;
import com.autoformattion.TDDspring.business.ItemBusinessService;
import com.autoformattion.TDDspring.model.Item;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ItemController.class)
public class ItemControllerTest {
    
    @Autowired
    private MockMvc mockMvc; //this is the server side

    @MockBean
    private ItemBusinessService businessService; //we get data from here

    @Test
    public void dummy_item() throws Exception {

        RequestBuilder request = MockMvcRequestBuilders
                    .get("/dummy-item")
                    .accept(MediaType.APPLICATION_JSON);
        
        MvcResult result = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"name\":\"Ball\",\"price\":10,\"quantity\":100}"))
                .andReturn();
    }

    @Test
    public void itemFromBusinessServicebasic() throws Exception  {

        //mocking method
        when(businessService.retrieveHardcodedItem()).thenReturn(
            new Item(2, "Item2", 10, 100));

        //building request
        RequestBuilder request = MockMvcRequestBuilders
                .get("/item-from-business-service")
                .accept(MediaType.APPLICATION_JSON);
        
        //testing results
        MvcResult result = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("{id: 2,name:Item2,price:10}"))
                .andReturn();
    }

    @Test
    public void retrieveAllItems_basic() throws Exception{
        //step1: mock method that will be used which is here retrieveAllItems
        when(businessService.retrieveAllItems()).thenReturn(
            Arrays.asList(new Item(2,"Item2",20,20), 
                    new Item(3,"Item3",30,30))
        );

        //step2: build API
        RequestBuilder request = MockMvcRequestBuilders
                .get("/all-items-from-database")
                .accept(MediaType.APPLICATION_JSON);

        // ask server side and compare results
        MvcResult result = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("[{id:3, name:Item3,price:30},{id:2, name:Item2,price:20}]"))
                .andReturn();

    }
}
