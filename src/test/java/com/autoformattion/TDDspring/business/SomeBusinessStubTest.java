package com.autoformattion.TDDspring.business;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.autoformattion.TDDspring.data.SomeDataService;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**Three stubs, not maintainable 
 * Stubs are big head ache to maintain
*/
class SomeDataServiceStub implements SomeDataService{

    @Override
    public int[] retrieveAllData() {
        // TODO Auto-generated method stub
        return new int[] {1,2,3};
    }
    
}

class SomeDataServiceOneElemenStub implements SomeDataService{

    @Override
    public int[] retrieveAllData() {
        // TODO Auto-generated method stub
        return new int[]{5};
    }
    
}

class SomeDataServiceEmptyElemenStub implements SomeDataService{

    @Override
    public int[] retrieveAllData() {
        // TODO Auto-generated method stub
        return new int[]{};
    }
    
}

public class SomeBusinessStubTest {
    
    @Test
    @DisplayName("the sum of data elements")
    public void calculateSumUsingDataService_basic() {

        SomeBusinessImpl business = new SomeBusinessImpl();
        business.setSomeDataService(new SomeDataServiceStub());

        int actualResutl = business.calculateSumUsingDataService();
        int expectedResult = 6;
        assertEquals(expectedResult, actualResutl);
    }

    @Test
    @DisplayName("the sum of data elements")
    public void calculateSum_empty() {
        SomeBusinessImpl business = new SomeBusinessImpl();
        business.setSomeDataService(new  SomeDataServiceEmptyElemenStub());

        int actualResutl = business.calculateSumUsingDataService();
        int expectedResult = 0;
        assertEquals(expectedResult, actualResutl);
    }

    @Test
    @DisplayName("the sum of data elements")
    public void calculateSum_oneValue() {
        SomeBusinessImpl business = new SomeBusinessImpl();
        business.setSomeDataService(new SomeDataServiceOneElemenStub());
        int actualResutl = business.calculateSumUsingDataService();
        int expectedResult = 5;
        assertEquals(expectedResult, actualResutl);
    }
}
