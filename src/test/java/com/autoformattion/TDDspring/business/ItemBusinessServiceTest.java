package com.autoformattion.TDDspring.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.autoformattion.TDDspring.data.ItemRepository;
import com.autoformattion.TDDspring.model.Item;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

//Spring test is not needed
@ExtendWith(MockitoExtension.class)
public class ItemBusinessServiceTest {

    //Let mockito initiate business object for us, 
    //like @autowired in Spring
    @InjectMocks
    ItemBusinessService itemBusinessService;

    //Mock itemRepository
    @Mock
    ItemRepository itemRepository;

    @Test
    void retrieveAllItems_basic() {
        when(itemRepository.findAll()).thenReturn(
            Arrays.asList(new Item(1,"item1",10,10), new Item(2,"item2",20,20))
        );

        List<Item> items = itemBusinessService.retrieveAllItems();

        assertEquals(100, items.get(0).getValue());
        assertEquals(400, items.get(1).getValue());
    }
}
