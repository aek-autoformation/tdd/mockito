package com.autoformattion.TDDspring.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.autoformattion.TDDspring.data.SomeDataService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SomeBusinessMockTest {

    //dataServiceMock will be injected
    @InjectMocks
    SomeBusinessImpl business;
    
    //same as moke(dataServiceMock)
    @Mock
    SomeDataService dataServiceMock;


    @Test
    @DisplayName("the sum of data elements")
    public void calculateSumUsingDataService_basic() {
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{1,2,3});
        assertEquals(6, business.calculateSumUsingDataService()); 
    }


    @Test
    @DisplayName("the sum of empty data")
    public void calculateSumUsingDataService_empty() {
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{});
        assertEquals(0, business.calculateSumUsingDataService()); 
    }


    @Test
    @DisplayName("the sum of one element data")
    public void calculateSumUsingDataService_onevalue() {
        when(dataServiceMock.retrieveAllData()).thenReturn(new int[]{5});
        assertEquals(5, business.calculateSumUsingDataService()); 
    }
    
}
