package com.autoformattion.TDDspring.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ListMockTest {

    @Mock
    List<String> mock;
    
    @Test
    public void size_basic(){
        when(mock.size()).thenReturn(5);
        assertEquals(5, mock.size());
    }

    @Test
    public void returnDifferentValues(){
        when(mock.size()).thenReturn(5).thenReturn(10);
        assertEquals(5, mock.size());
        assertEquals(10, mock.size());
    }

    @Test
    public void returnWithParameters(){
        when(mock.get(0)).thenReturn("iliass");
        assertEquals("iliass", mock.get(0));
        assertEquals(null, mock.get(1));
    }

    @Test
    public void returnWithGenericParameters(){
        when(mock.get(anyInt())).thenReturn("Iliass");
        assertEquals("Iliass", mock.get(0));
        assertEquals("Iliass", mock.get(1));
    }

    @Test
    public void verificationBasics(){
        String value1 = mock.get(0);
        String value2 = mock.get(1);

        // checks that .get(0) was called once
        verify(mock).get(0);

        // checks the .get(int) was called two times
        verify(mock, times(2)).get(anyInt());

        verify(mock, atLeast(1)).get(anyInt());

        verify(mock, atMost(2)).get(anyInt());

        verify(mock, never()).get(2);
    }

    @Test
    public void argumentCapturing() {
        mock.add("SomeString");

        //Define captor
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        
        //Capturing argument is necessary for captor.getValue()
        verify(mock).add(captor.capture());
        assertEquals("SomeString", captor.getValue());
    }
    
    @Test
    @DisplayName("Capture and verify arguments for multiple argument calls")
    public void multipleArgumentCapturing(){
        mock.add("SomeString1");
        mock.add("SomeString2");

        /**Verification */

        // initialize captor for String class argument
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        //Verify add method is calles exactly two times
        verify(mock, times(2)).add(captor.capture());

        //Collect all arguments
        List<String> allValues = captor.getAllValues();
        
        // test arguments collected
        assertEquals("SomeString1", allValues.get(0));
        assertEquals("SomeString2", allValues.get(1));

    }

    @Test
    public void Mocking(){
        ArrayList arrayListMock = mock(ArrayList.class);

        arrayListMock.get(0);//null
        arrayListMock.size();//0

        arrayListMock.add("Test");
        arrayListMock.add("Test2");
        arrayListMock.size();//0

        when(arrayListMock.size()).thenReturn(5);
        arrayListMock.size(); //5
    }


    @Test
    public void Spying(){
        ArrayList arrayListSpy = mock(ArrayList.class);

        arrayListSpy.add("Test0");
        arrayListSpy.get(0);//Test0
        arrayListSpy.add("Test1");
        arrayListSpy.add("Test2");
        arrayListSpy.size();//3

        /**All code above will be lost */
        when(arrayListSpy.size()).thenReturn(5);
        arrayListSpy.size();//5

        System.out.println(arrayListSpy.get(0));//null
    }
}
