package com.autoformattion.TDDspring.business;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SomeBusinessTest {
    
    @Test
    @DisplayName("the sum of data elements")
    public void calculateSum_basic() {
        SomeBusinessImpl business = new SomeBusinessImpl();
        int actualResutl = business.caclulateSum(new int[]{1,2,3});
        int expectedResult = 6;
        assertEquals(expectedResult, actualResutl);
    }

    @Test
    @DisplayName("the sum of data elements")
    public void calculateSum_empty() {
        SomeBusinessImpl business = new SomeBusinessImpl();
        int actualResutl = business.caclulateSum(new int[]{1,2,3});
        int expectedResult = 6;
        assertEquals(expectedResult, actualResutl);
    }

    @Test
    @DisplayName("the sum of data elements")
    public void calculateSum_oneValue() {
        SomeBusinessImpl business = new SomeBusinessImpl();
        int actualResutl = business.caclulateSum(new int[]{5});
        int expectedResult = 5;
        assertEquals(expectedResult, actualResutl);
    }
}
